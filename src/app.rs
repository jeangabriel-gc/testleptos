use leptos::*;
use leptos_meta::*;
use leptos_router::*;

#[component]
pub fn TestApp(cx: Scope) -> impl IntoView {
    // Provides context that manages stylesheets, titles, meta tags, etc.
    provide_meta_context(cx);

    view! {
        cx,

        // injects a stylesheet into the document <head>
        // id=leptos means cargo-leptos will hot-reload this stylesheet
        <Stylesheet id="leptos" href="/pkg/leptos_start.css"/>

        // sets the document title
        <Title text="Welcome to Leptos"/>

        // content for this welcome page
        <Router>
            <header>
                <h1 class="text-3xl underline text-left">"NationTech HiveCloud"</h1>
            </header>
            <nav>
            <ul>
               <li> <A class="text-xs" href="form">"Form"</A></li>
               <li> <A class="underline" href="button">"Button"</A></li>
            </ul>
            </nav>
            <main class="border-4">
                <Routes>
                    <Route path="" view=|cx| view! {
                        cx,
                        <div>"Choose a path"</div>
                    }/>
                    <Route path="form" view=|cx| view! {
                        cx,
                        <HostForm />
                    }/>
                    <Route path="button" view=|cx| view! {
                        cx,
                        <ButtonAction />
                    }/>
                </Routes>
            </main>
        </Router>
    }
}

#[component]
pub fn ButtonAction(cx: Scope) -> impl IntoView {
    // let add_host = create_action(cx, |_| add_host(432));
    view! {
        cx,
        <div>
            <div>"Hello I am a button wtf"</div>
            // <button on:click=move |_| add_host.dispatch(())>"Hello action button"</button>
        </div>
    }
}

#[component]
pub fn HostForm(cx: Scope) -> impl IntoView {
    let test_host_param = create_server_action::<TestHostParam>(cx);
    let test_number_param = create_server_action::<TestNumberParam>(cx);
    view! {
        cx,
        <div>"HostForm"</div>
        <ActionForm action=test_host_param>
            <input type="hidden" name="host" value=12 />
            <input type="submit" value="Test form with host param FAILS locks up cpu to 100%" />
        </ActionForm>
        <ActionForm action=test_number_param>
            <input type="hidden" name="host" value=12 />
            <input type="submit" value="Test form number with host input name it FAILS" />
        </ActionForm>
        <ActionForm action=test_number_param>
            <input type="hidden" name="number" value=12 />
            <input type="submit" value="Test form with number param works" />
        </ActionForm>
    }
}


#[server(TestHostParam, "/api")]
pub async fn test_host_param(host: u16) -> Result<(), ServerFnError> {
    println!("Test form alloooo {}", host);
    Ok(())
}


#[server(TestNumberParam, "/api")]
pub async fn test_number_param(number: u16) -> Result<(), ServerFnError> {
    println!("add host paul?? a {}", number);
    Ok(())
}


#[cfg(feature = "ssr")]
pub fn register_server_functions() {
    _ = TestHostParam::register();
    _ = TestNumberParam::register();
    println!("registered functions");
}
