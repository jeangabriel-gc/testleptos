use leptos::*;
use cfg_if::cfg_if;

pub mod app;

cfg_if! {
if #[cfg(feature = "hydrate")] {

  use wasm_bindgen::prelude::wasm_bindgen;
  use crate::app::*;

    #[wasm_bindgen]
    pub fn hydrate() {
      use app::*;
      use leptos::*;

      // initializes logging using the `log` crate
      _ = console_log::init_with_level(log::Level::Debug);
      console_error_panic_hook::set_once();

      mount_to_body(|cx| {
          view! { cx, <TestApp/> }
      });
    }
}
}
